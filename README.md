# GamgeeElixir

At this point, this just passes messages from the router to the app and vice versa via websockets.

Not yet implemented is the handshake protocol or how the router identifies itself to the backend.

## Installation
You need to have [Elixir and Erlang](https://elixir-lang.org/install.html) installed.

### Install dependencies
`mix deps.get`

### Install dependencies and compile
`mix deps.compile`

## Run
`iex -S mix`

## Useful for testing
[React based testing client](https://bitbucket.org/KevinGamgee/websocket-tester/src/master/)

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/gamgee_elixir](https://hexdocs.pm/gamgee_elixir).

