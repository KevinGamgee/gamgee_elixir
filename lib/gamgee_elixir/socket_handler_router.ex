defmodule GamgeeElixir.RouterHandler do
  @behaviour :cowboy_websocket

  alias GamgeeElixir.Observer

  def init(request, _state) do
    IO.puts "Router request.path #{request.path} #{request.headers["origin"]}"
    # Maybe there's a smarter way to do this than using origin. Mac address would be nice
    state = %{origin: request.headers["origin"]}

    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    IO.puts "Router Socket Init"
    %{origin: origin} = state
    # This needs to be called from here to have access to the right pid
    Observer.add_update_router(%{ pid: self(), origin: origin })

    {:ok, state}
  end

  # Handle each message
  def websocket_handle({:text, json}, state) do

    IO.puts "#{json}"
    case Jason.decode(json) do
      {:ok, message} ->
        Observer.router_to_app(%{ pid: self(), message: message })
        {:reply, {:text, Jason.encode!("{'received':'router'}") }, state}
      _ ->
        {:reply, {:text, Jason.encode!("{'received':'router error'}") }, state}
    end

  end

  # Handle messages from Erlang/Elixir
  def websocket_info(info, _state) do
    {:reply, {:text, info}, []}
  end

  def send_message(pid, message) do
    Process.send(pid, Jason.encode!(message), [])
  end

end
