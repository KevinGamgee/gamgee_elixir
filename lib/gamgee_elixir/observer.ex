defmodule GamgeeElixir.Observer do

  alias GamgeeElixir.AppHandler
  alias GamgeeElixir.RouterHandler

  def start_link(_) do

    IO.puts("Observer running...")
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)

  end

  @doc """
  This needs to be here to satisfy Elixir
  """
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  @doc """
  This gets the server initialized
  """
  def init({}) do
    # init state
    {:ok, %{:nonces => [], :clients => [], :routers => []}}
  end

  @doc """
  This is called when a client is added
  """
  def add_update_client(client_info) do
    GenServer.cast(__MODULE__, {:add_update_client, client_info})
  end

  @doc """
  When a client gets a nonce from POST it gets catalogued here
  """
  def add_nonce(nonce) do
    GenServer.cast(__MODULE__, {:add_nonce, nonce})
  end

  @doc """
  The router calls the backend
  """
  def add_update_router(router_info) do
    GenServer.cast(__MODULE__, {:add_update_router, router_info})
  end

  # Message from router to app
  def router_to_app(from_router) do
    GenServer.cast(__MODULE__, {:router_to_app, from_router})
  end

  # Message from app to router
  def app_to_router(from_app) do
    GenServer.cast(__MODULE__, {:app_to_router, from_app})
  end

  def handle_cast({:add_update_client, new_client}, state) do
    %{:clients => existing_clients} = state

    new_clients = merge_clients(existing_clients, new_client)

    new_state = Map.merge(state, %{:clients => new_clients})
    IO.puts("Clients connected... #{length(new_clients)}")

    {:noreply, new_state}
  end

  def handle_cast({:add_nonce, nonce}, state) do
    %{:nonces => existing_nonces} = state

    new_nonces = [nonce | existing_nonces]
    new_state = Map.merge(state, %{:nonces => new_nonces})
    IO.puts("Number of nonces... #{length(new_nonces)}")

    {:noreply, new_state}
  end

  def handle_cast({:add_update_router, new_router}, state) do
    %{routers: existing_routers} = state

    new_routers = merge_routers(existing_routers, new_router)

    new_state = Map.merge(state, %{:routers => new_routers})
    IO.puts("Number of routers... #{length(new_routers)}")

    {:noreply, new_state}
  end

  def handle_cast({:router_to_app, from_router}, state) do

    %{message: message } = from_router
    %{clients: clients} = state
    IO.puts "Router message to client #{inspect clients}"
    %{pid: pid} = Enum.at(clients, 0)
    IO.puts "Router message to client #{inspect pid}"
    AppHandler.send_message(pid, message)
    {:noreply, state}
  end

  def handle_cast({:app_to_router, from_app}, state) do

    %{message: message } = from_app
    %{routers: routers} = state
    IO.puts "App message to router #{inspect routers}"
    %{pid: pid} = Enum.at(routers, 0)
    IO.puts "App message to router #{inspect pid}"
    RouterHandler.send_message(pid, message)
    {:noreply, state}
  end

  # def receiveRouter () do
  #   receive do
  #     {sender, value} ->
  #       # code
  #   end

  # end

  @doc """
  TODO Put in a way to remove a client
  """

  @doc """
  TODO Put in a way to remove a nonce
  """

  # New client from empty list or adding to existing list
  def merge_clients([], new_client) do
    [new_client]
  end

  # Replace existing client to update pid
  def merge_clients([h | tail_clients], new_client) do
    cond do
      # Replace if they have the same origin
      h.origin == new_client.origin ->
        [new_client | tail_clients]
      # Go down the list
      true ->
        [h | merge_clients(tail_clients, new_client)]
    end
  end

  # New router from empty list or adding to existing list
  def merge_routers([], new_router) do
    [new_router]
  end

  # Replace existing router to update pid
  def merge_routers([h | tail_routers], new_router) do
    cond do
      # Replace if they have the same origin
      h.origin == new_router.origin ->
        [new_router | tail_routers]
      # Go down the list
      true ->
        [h | merge_routers(tail_routers, new_router)]
    end
  end

end
