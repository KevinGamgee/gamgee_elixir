defmodule GamgeeElixir.AppHandler do
  @behaviour :cowboy_websocket

  alias GamgeeElixir.Observer

  def init(request, _state) do
    IO.puts "Client request.headers #{inspect request.path} #{inspect request.headers["origin"]}"
    # Maybe there's a smarter way to do this than using origin. Mac address would be nice
    state = %{origin: request.headers["origin"]}

    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    %{origin: origin} = state
    # This needs to be called from here to have access to the websocket pid
    Observer.add_update_client(%{ pid: self(), origin: origin })

    {:ok, []}
  end

  # Handle each message
  def websocket_handle({:text, json}, _state) do

    IO.puts "#{json}"
    case Jason.decode(json) do
      {:ok, message} ->
        Observer.app_to_router(%{ pid: self(), message: message })
        {:reply, {:text, Jason.encode!("{'received':'client'}") }, []}
      _ ->
        {:reply, {:text, Jason.encode!("{'received':'client error'}") }, []}
    end
  end

  # Handle messages from Erlang/Elixir
  def websocket_info(info, _state) do
    {:reply, {:text, info}, []}
  end

  def send_message(pid, message) do
    Process.send(pid, Jason.encode!(message), [])
  end

end
