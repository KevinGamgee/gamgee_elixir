defmodule GamgeeElixir.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    obs_args = []

    # List all child processes to be supervised
    children = [
      # Observer is independent so if the main server crashes Observer persists,
      {GamgeeElixir.Observer, obs_args},
      {
        Plug.Cowboy,
        scheme: :http,
        # This is where all of the http action is
        plug: GamgeeElixir.Endpoint,

        options: [
          # Specify the port as per runtime specs
          port: Application.get_env(:gamgee_elixir, :port),
          dispatch: dispatch(),
        ]
      },
      # Used to identify the websocket endpoints
      Registry.child_spec(
        keys: :duplicate,
        name: Registry.GamgeeElixir
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GamgeeElixir.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {:_,
        [
          # Endpoint for app mapped to handler
          {"/ws/[...]", GamgeeElixir.AppHandler, []},
          # Endpoint for router mapped to handler
          {"/router/[...]", GamgeeElixir.RouterHandler, []},
          {:_, Plug.Cowboy.Handler, {GamgeeElixir.Endpoint, []}}
        ]
      }
    ]
  end

end
