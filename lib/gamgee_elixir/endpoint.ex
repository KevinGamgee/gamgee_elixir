defmodule GamgeeElixir.Endpoint do

  alias GamgeeElixir.Observer

  use Plug.Router

  # This is the parser we'll use
  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason

  # Using Plug.Logger for logging request information
  plug Plug.Logger
  # responsible for matching routes
  plug :match
  # responsible for dispatching responses
  plug :dispatch

  # Initial part of handshake
  post "/auth/developer/callback" do
    send_resp(conn, 200, get_nonce())
  end

  # Just for testing verification
  get "/nonce" do
    IO.puts "Nonce #{Mix.env()}"
    case Mix.env() do
      :prod ->
        send_resp(conn, 404, "404")
      _ ->
        nonce = get_nonce()
        Observer.add_nonce(nonce)
        send_resp(conn, 200, nonce)
    end
  end

  # Application.get_env(:gamgee_elixir, :version)
  # This is how the app starts the handshake
  post "/nonce" do
    nonce = get_nonce()
    Observer.add_nonce(nonce)
    send_resp(conn, 200, nonce)
  end

  defp get_nonce() do
    String.pad_leading(Integer.to_string(:random.uniform(9999999999)), 10, ["0"])
  end

  # A simple route to test that the server is up
  # Note, all routes must return a connection as per the Plug spec.
  get "/" do
    index = File.read!("priv/static/index.html")
    send_resp(conn, 200, index)
  end

  # A catchall route, 'match' will match no matter the request method,
  # so a response is always returned, even if there is no route to match.
  match _ do
    IO.puts "404"
    send_resp(conn, 404, "I thought I died and went to heaven... but it was just 404.")
  end

end
